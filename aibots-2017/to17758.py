from player import Bot
import random


class To17758(Bot):
    def __init__(self, game, index, spy):
        super(To17758, self).__init__(game, index, spy)
        self.game_blackboard = GameBlackboard

    def onGameRevealed(self, players, spies):
        self.spies = spies
        self.player_mission_results = {p.name: 0 for p in players}
        self.game_stats = GameStats
        self.game_stats.players = players

    def onTeamSelected(self, leader, team):
        self.game_stats.leaders.append(leader)
        self.game_stats.teams.append(team)

    def select(self, players, count):
        if self.spy:
            return [self] + random.sample([p for p in self.others() if p not in self.spies], count - 1)
        else:
            sorted_players = sorted(self.player_mission_results, key=self.player_mission_results.get)
            selected_players = [self]
            i = 0
            while len(selected_players) != count:
                selected_players = selected_players + [p for p in self.others() if p.name == sorted_players[i]]
                i += 1
            return selected_players

    def onVoteComplete(self, votes):
        self.game_stats.votes.append(votes)

    def vote(self, team):
        if self.spy:
            return len([s for s in team if s in self.spies]) >= 1
        else:
            return True

    def sabotage(self):
        # if it is the first round, choose randomly how to vote to throw other players of guard
        # not helpful...
        # if self.game.tries == 1:
        #     return bool(random.getrandbits(1))
        return True

    def onMissionComplete(self, sabotaged):
        if sabotaged > 0:
            for player in [p for p in self.game.team if p != self]:
                self.player_mission_results[player.name] = self.player_mission_results[player.name] + 1

    def onGameComplete(self, win, spies):
        self.game_stats.spies = spies
        self.game_stats.resistance_won = win
        self.game_blackboard.game_stats.append(self.game_stats)

class GameStats:
    def __init__(self):
        pass

    players = []
    teams = []
    votes = []
    leaders = []
    spies = []
    resistance_won = False


class GameBlackboard:
    def __init__(self):
        pass

    game_stats = []
    player_stats = []


class PlayerStats:
    def __init__(self):
        pass

    name = ""
    # did they always vote to sabotage if they were on a mission
    # did they sabotage on the first round
    # did they behave differently on the last mission
